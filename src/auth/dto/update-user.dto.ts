import { IsEmail, IsNotEmpty, isNotEmpty, IsOptional } from "class-validator"

export class UpdateUserDTO {

    @IsOptional()
    @IsNotEmpty()
    first_name: string

    @IsOptional()
    @IsNotEmpty()
    last_name: string 
    
    @IsEmail()
    @IsNotEmpty() 
    email?: string     // ? the same as IsOptional
    
}