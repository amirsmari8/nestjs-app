import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { UserModule } from '../user/user.module'; 
import { JwtModule } from '@nestjs/jwt';
import { AuthGuard } from './auth.guard'

@Module({
  controllers: [AuthController],
  imports: [ UserModule, 
  JwtModule.register({                // jwt module exports jwt service in backgound 
    secret: 'secret', 
    signOptions: { expiresIn: '1d' }
  }) ], 
  exports: [JwtModule]
  
})
export class AuthModule {}
