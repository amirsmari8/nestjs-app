import { BadRequestException, Body,  ClassSerializerInterceptor,  Controller, Get, Post, Req, Res, UseGuards, UseInterceptors } from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';
import { RegisterDTO } from './dto/register.dto'
import { JwtService } from '@nestjs/jwt';
import { Request, Response } from 'express'
import { AuthGuard } from './auth.guard';


@UseInterceptors(ClassSerializerInterceptor)   // ClassSerializerInterceptor(intercept the outgoing response) : know about the @Exclude() in user entity to exclude the password
@Controller()
export class AuthController {
    constructor(private userService: UserService,
        private jwtService: JwtService){}

    @Post() 
    async register(@Body() body:RegisterDTO){

        if (body.password !== body.confirme_password) {
            throw new BadRequestException (`password don't match`)
        }

        const hashedPass =  await bcrypt.hash(body.password, 12)
        const user = await this.userService.createUser({
            first_name : body.first_name ,
            last_name : body.last_name,
            password : hashedPass, 
            email : body.email          
        })        
        return user
    }

    @Post('login')
    async login(@Body('email') email: string, @Body('password') password: string, @Res({ passthrough: true }) response: Response ){  // to use httponly cookie @Res({passthrough: true}) 
        const user = await this.userService.findUser({email: email})
        if(!user){
            throw new BadRequestException ('user not found')
        }
        const hashedPass =  await bcrypt.compare(password, user.password) // it will return true or false
        if (!hashedPass){
            throw new BadRequestException(`password don't much , please verify it, invalid credentials`)
        }
        
        
        // working with jwt 
        const jwt = await this.jwtService.signAsync({id: user.id})  // the pyload contain only the id of the user

        response.cookie('jwt', jwt, {httpOnly: true}) // 'jwt': name of cookie, jwt: jwt token

        return user
    } 

    @UseGuards(AuthGuard)  // authorization
    @Get('current-user')
    async getCurrentUser(@Req() request: Request){
        // console.log(request.cookies)
        const cookie = request.cookies['jwt']      // return the token: eyJhbGciOiJIUz ...
        const data = await this.jwtService.verifyAsync(cookie) // return : { id: 3, iat: 1649273898, exp: 1649360298 }
        const user = await this.userService.findUser(data['id']) 
        return user 
    }

    @UseGuards(AuthGuard)  // authorization
    @Post('logout')
    async logour(@Res({ passthrough: true }) responsee: Response){
        responsee.clearCookie('jwt')
    }
    
}
