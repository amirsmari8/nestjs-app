import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './models/user.entity'


@Injectable()
export class UserService {
    constructor(@InjectRepository(User)  private repo: Repository<User>){} //repo will be an instance of type ORM repository){}
    
    async createUser(data): Promise<User>{
        return await this.repo.save(data)
    }

    async getUsers(): Promise<User[]>{
        return await this.repo.find()
    }

    async findUser(condition): Promise<User>{
        return await this.repo.findOne(condition)
    }

    async UpdateUser(id, data) {
        const user = await this.repo.update(id, data)
        return this.repo.findOne(id)
    }
    
}
