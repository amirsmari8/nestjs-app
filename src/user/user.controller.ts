import { Body, ClassSerializerInterceptor, Controller, Get, NotFoundException, Param, Post, Put, Req, UseGuards, UseInterceptors } from '@nestjs/common';
import { User } from './models/user.entity';
import { UserService } from './user.service';
import { Request } from 'express'
import { CreateUserDTO } from 'src/auth/dto/create-user.dto';
import * as bcrypt from 'bcrypt';
import { AuthGuard } from 'src/auth/auth.guard';
import { UpdateUserDTO } from '../auth/dto/update-user.dto'

@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(AuthGuard)
@Controller('users')
export class UserController {
    constructor(private userService: UserService){}
    
    @Get()
    async getAllUser(){
        return await this.userService.getUsers()
    }


    @Post() 
    async createUser(@Body() body: CreateUserDTO ): Promise<User>{
        const password = await bcrypt.hash('1234', 12)
        return  this.userService.createUser({
            first_name : body.first_name, 
            last_name : body.last_name, 
            email: body.email, 
            password
        })   
    }

    @Get('/:id') 
    async GetUser(@Param('id') id: string ){
        return this.userService.findUser(parseInt(id))
    }

    @Put('/:id')
    async updateUser(@Param('id') id:string, @Body() body: UpdateUserDTO){
        const user = await this.userService.findUser(parseInt(id))
        if(!user) {
            throw new NotFoundException(`the user with ${id} is not found`)
        }
        Object.assign(user, body)
        return this.userService.UpdateUser(parseInt(id), user)
    }
}
