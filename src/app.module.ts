import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config'
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [UserModule, 
    ConfigModule.forRoot({          //ConfigModule : is going to specify wich of the 2 file (.env.dev or.env.prod) we're going to read.
      isGlobal: true, 
      envFilePath: `.env.${process.env.NODE_ENV}`
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {     //ConfigService : to expose the information inside those file to the reset of the application
        return {
          type: 'postgres', 
          database: process.env.DB_NAME,
          port: 5000, 
          username: process.env.USERNAME, 
          password: process.env.PASSWORD, 
          host: process.env.HOST,
          autoLoadEntities: true,
          // Synchronize is true only for dev 
          // TODO: we need to pass this as envirnoment variable 
          synchronize: true, 
        }  
      }
    }),
    AuthModule,],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
