import { IsEmail, IsNotEmpty } from "class-validator"

export class CreateUserDTO {
    @IsNotEmpty()
    first_name: string
    
    @IsNotEmpty()
    last_name: string 
    
    @IsNotEmpty()
    @IsEmail()
    email: string
}